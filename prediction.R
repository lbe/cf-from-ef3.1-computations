prediction_HT_ET<-function(filename)
{  ##filename is a csv file with 41 columns (the CAS and the 40 DM)
  load("models/2023_12_07_pls.Rdata")

##classes dans  results$analyse$classes_mol
  rownames(results$analyse$classes_mol)<-results$analyse$classes_mol[,1]
  colnames(results$analyse$classes_mol)<-c("CAS","classes")
##donnees dans results$X
##loadings des individus dans results$analyse$regPLS$variates$X et results$analyse$regPLS$variates$Y
  library(class) #for knn
  test<-read.csv(filename,sep=";",dec=".")

##missranger
  library("mixOmics")
  X_miss<-results$X
  ##we change the name to have a simple format
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-0")]<-"C0"
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-1")]<-"C1"
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-2")]<-"C2"
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-3")]<-"C3"
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-4")]<-"C4"
  colnames(X_miss)[which(colnames(X_miss)=="Connectivity index chi-5")]<-"C5"
  colnames(X_miss)[which(colnames(X_miss)== "Electric dipole moment" )]<-"Elec_dip_mom"
  colnames(X_miss)[which(colnames(X_miss)=="HOMO energy")]<-"HOMO"
  colnames(X_miss)[which(colnames(X_miss)== "LUMO energy" )]<-"LUMO"
  colnames(X_miss)[which(colnames(X_miss)=="Molecular mass" )]<-"Molecular_mass" 
  colnames(X_miss)[which(colnames(X_miss)=="Molecular surface area (Connolly)")]<-"Molec_surface_area"
  colnames(X_miss)[which(colnames(X_miss)=="Number of Carbon atoms")]<- "Nb_C"
  colnames(X_miss)[which(colnames(X_miss)=="Number of Chlorine atoms")]<- "Nb_Chl"  
  colnames(X_miss)[which(colnames(X_miss)=="Number of Fluorine atoms")]<- "Nb_Fluo"
  colnames(X_miss)[which(colnames(X_miss)=="Number of Hydrogen atoms")]<-"Nb_H" 
  colnames(X_miss)[which(colnames(X_miss)=="Number of Nitrogen atoms")]<-"Nb_Nitro" 
  colnames(X_miss)[which(colnames(X_miss)=="Number of Oxygen atoms")]<-"Nb_O" 
  colnames(X_miss)[which(colnames(X_miss)=="Number of Phosphorus atoms")]<-"Nb_Phos"
  colnames(X_miss)[which(colnames(X_miss)=="Number of Sulfur atoms")]<-"Nb_S"  
  colnames(X_miss)[which(colnames(X_miss)=="Number of aromatic bonds")]<-"Nb_aromatic_bonds"   
  colnames(X_miss)[which(colnames(X_miss)== "Number of atoms")]<-"Nb_atoms" 
  colnames(X_miss)[which(colnames(X_miss)=="Number of bonds")]<-"Nb_bonds" 
  colnames(X_miss)[which(colnames(X_miss)=="Number of circuits")]<-"Nb_circuits"
  colnames(X_miss)[which(colnames(X_miss)=="Number of double bonds")]<-"Nb_double_bonds"
  colnames(X_miss)[which(colnames(X_miss)=="Number of halogen atoms")]<-"Nb_halogen"
  colnames(X_miss)[which(colnames(X_miss)=="Number of multiple bonds")]<-"Nb_multiple_bonds"
  colnames(X_miss)[which(colnames(X_miss)=="Number of non-H atoms")]<-"Nb_non_H_atoms"
  colnames(X_miss)[which(colnames(X_miss)=="Number of non-H bonds")]<-"Nb_non_H_bonds"
  colnames(X_miss)[which(colnames(X_miss)=="Number of rings")]<-"Nb_rings"
  colnames(X_miss)[which(colnames(X_miss)=="Number of rotatable bonds")]<-"Nb_rotatable_bonds"
  colnames(X_miss)[which(colnames(X_miss)=="Number of triple bonds")]<-"Nb_triple_bonds"
  colnames(X_miss)[which(colnames(X_miss)=="Polarizability")]<-"Pola"
  colnames(X_miss)[which(colnames(X_miss)=="Sum of conventional bond order")]<-"Sum_of_conv"
  colnames(X_miss)[which(colnames(X_miss)=="Ionization Potential")]<-"Ioni_Pot"
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-0")]<-"V0"
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-1")]<-"V1"
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-2")]<-"V2" 
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-3")]<-"V3"
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-4")]<-"V4" 
  colnames(X_miss)[which(colnames(X_miss)=="Valence connectivity index chi-5")]<-"V5"
  
  if (!all(colnames(X_miss)%in%colnames(test))) {stop('Check the format of your csv file. Please use the same column names of the example.')}
  col.order<-colnames(X_miss)

  ##imputation of missing values in the inputs
  if (any(is.na(test[,col.order])))
  { library(missRanger)
    total_tmp<-rbind(test[,col.order],X_miss)
    test<-cbind(test$CAS,rowSums(is.na(test)),missRanger(total_tmp, num.trees = 1000, verbose = 0)[1:nrow(test),])
  }
  colnames(test)[which(colnames(test)=="test$CAS")]<-"CAS"
  colnames(test)[which(colnames(test)=="rowSums(is.na(test))")]<-"Nb_NA"
##prediction PLS
  test_tmp<-test[,col.order]
  colnames(test_tmp)<-colnames(results$analyse$regPLS$X)
  test_pls<-predict(results$analyse$regPLS,newdata=test_tmp)


##prediction cluster using knn
  test$classe<-knn(results$analyse$regPLS$variates$X,test_pls$variates,results$analyse$classes_mol[,2],k=7,prob=TRUE)
  ##k=sqrt(n) too big to let a chance for small clusters (local structure assumption)
  
##Prediction
  ########
  library(ranger)
  library(e1071)
  library(tuneRanger)
  test$CF_HTnoncanc<-NA
  test$CF_ET<-NA
  test$CF_HTcanc<-NA

  if (sum(test$classe==1)>0)
  { load("models/model_svm_ET_cl1.Rdata") 
    test$CF_ET[test$classe==1]<-predict(model_svm_ET_cl1,test[test$classe==1,names(model_svm_ET_cl1$x.scale$`scaled:center`)]) ##ET
    load("models/model_svm_HT_noncanc_cl1_3_4_6.Rdata")  
    test$CF_HTnoncanc[test$classe==1]<-predict(model_svm_HT_noncanc_cl1_3_4_6,test[test$classe==1,names(model_svm_HT_noncanc_cl1_3_4_6$x.scale$`scaled:center`)]) ##HT
  }

  if (sum(test$classe==2)>0)
  { load("models/model_svm_ET_cl2_3_6.Rdata") 
    test$CF_ET[test$classe==2]<-predict(model_svm_ET_cl2_3_6,test[test$classe==2,names(model_svm_ET_cl2_3_6$x.scale$`scaled:center`)]) ##ET
    load("models/model_svm_HT_noncanc_cl2.Rdata")
    test$CF_HTnoncanc[test$classe==2]<-predict(model_svm_HT_noncanc_cl2,test[test$classe==2,names(model_svm_HT_noncanc_cl2$x.scale$`scaled:center`)]) ##HT
    }
  
  if (sum(test$classe==3)>0)
  { load("models/model_svm_ET_cl2_3_6.Rdata") 
    test$CF_ET[test$classe==3]<-predict(model_svm_ET_cl2_3_6,test[test$classe==3,names(model_svm_ET_cl2_3_6$x.scale$`scaled:center`)]) ##ET
    load("models/model_svm_HT_noncanc_cl1_3_4_6.Rdata")  
    test$CF_HTnoncanc[test$classe==3]<-predict(model_svm_HT_noncanc_cl1_3_4_6,test[test$classe==3,names(model_svm_HT_noncanc_cl1_3_4_6$x.scale$`scaled:center`)]) ##HT
     }
  
  if (sum(test$classe==4)>0)
  { load("models/model_rf_ET_cl4.Rdata") 
    test$CF_ET[test$classe==4]<-predict(model_rf_ET_cl4$model,newdata=test[test$classe==4,])$data$response ##ET
    load("models/model_svm_HT_noncanc_cl1_3_4_6.Rdata")  
    test$CF_HTnoncanc[test$classe==4]<-predict(model_svm_HT_noncanc_cl1_3_4_6,test[test$classe==4,names(model_svm_HT_noncanc_cl1_3_4_6$x.scale$`scaled:center`)]) ##HT
     }
  
  if (sum(test$classe==5)>0)
  { load("models/model_rf_ET_cl5.Rdata") 
    test$CF_ET[test$classe==5]<-predict(model_rf_ET_cl5$model,newdata=test[test$classe==5,])$data$response ##ET
    load("models/model_rf_HT_noncanc_cl5.Rdata")  
    test$CF_HTnoncanc[test$classe==5]<-predict(model_rf_HT_noncanc_cl5$model,newdata=test[test$classe==5,])$data$response
  }
  
  if (sum(test$classe==6)>0)
  { load("models/model_svm_ET_cl2_3_6.Rdata") 
    test$CF_ET[test$classe==6]<-predict(model_svm_ET_cl2_3_6,test[test$classe==6,names(model_svm_ET_cl2_3_6$x.scale$`scaled:center`)]) ##ET
    load("models/model_svm_HT_noncanc_cl1_3_4_6.Rdata")  
    test$CF_HTnoncanc[test$classe==6]<-predict(model_svm_HT_noncanc_cl1_3_4_6,test[test$classe==6,names(model_svm_HT_noncanc_cl1_3_4_6$x.scale$`scaled:center`)]) ##HT
  }
  
  load("models/knn_HT_canc.Rdata")  
  load("models/model_RF_HT_canc.Rdata")
  test$CF_HTcanc<-predict(model_RF_HT_canc$model,newdata=test)$data$response
  col.order.knn<-colnames(data_knn_HTcanc)[3:42]
  HT_canc_null<-knn(data_knn_HTcanc[,c(3:42)],test[,col.order.knn],data_knn_HTcanc$Pre_cluster,k=round(sqrt(nrow(data_knn_HTcanc))))
  test$CF_HTcanc[which(HT_canc_null==TRUE)]<--10
  
  test$CF_HTnoncanc<-10**(test$CF_HTnoncanc)
  test$CF_ET<-10**test$CF_ET
  test$CF_HTcanc<-(10**test$CF_HTcanc)-1e-10

  sortie<-data.frame("CAS"=test$CAS,"Cluster"=test$classe,"Nb MD missing values"=test$Nb_NA,"CF Eco Tox"=round(test$CF_ET,digits=0),
                     "CF Human Tox non cancer"=signif(test$CF_HTnoncanc,digits=3),"CF Human Tox cancer"=signif(test$CF_HTcanc,digits=3))
  write.csv(sortie,file=paste("results_",filename,sep=""))
  
}